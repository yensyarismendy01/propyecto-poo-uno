<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['middleware' => ['auth']], function(){
  
    Route::post('/Guardar/clientes' , ['as' => 'guardar.cliente', 'uses' => 'ClienteController@GuardarCliente']);
    Route::get('/Listar/clientes' , ['as' => 'List.cliente', 'uses' => 'ClienteController@InicioCliente']);
    Route::get('/crear/clientes' , ['as' => 'crear.cliente', 'uses' => 'ClienteController@CrearCliente']);


    Route::post('/Guardar/productos' , ['as' => 'guardar.producto', 'uses' => 'ProductoController@GuardarProducto']);
    Route::get('/Listar/productos' , ['as' => 'List.producto', 'uses' => 'ProductoController@InicioProducto']);
    Route::get('/crear/productos' , ['as' => 'crear.producto', 'uses' => 'ProductoController@CrearProducto']);



});