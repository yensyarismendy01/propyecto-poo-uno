<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

//SE DECLARAN LOS ATRIBUTOS EN LA CLASE 
class producto extends Model
{
    protected $fillable = [
        'NOMBRE', 
        'TIPO',
        'ESTADO',
        'PRECIO',
    ];
}