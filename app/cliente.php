<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

//SE DECLARAN LOS ATRIBUTOS EN LA CLASE 
class cliente extends Model
{
    protected $fillable = [
        'NOMBRE', 
        'APELLIDOS',
        'CEDULA',
        'DIRECCION',
        'TELEFONO',
        'FECHA_DE_NACIMIENTO',
        'EMAIL',
    ];
}