<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\cliente;

class ClienteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function InicioCliente(Request $request)
    {
       // dd('hola mundo');
       $cliente = cliente::all();
       //dd($cliente);
       return view('cliente.inicio') -> with('cliente',$cliente);
    }

    public function CrearCliente(Request $request)
    {
       $cliente = cliente::all();
       return view('cliente.crear')->with('cliente',$cliente);
    }

    public function GuardarCliente(Request $request)
    {
       $this->validate($request,[
        'NOMBRE' => 'required',
        'APELLIDOS' => 'required',
        'CEDULA' => 'required',
        'DIRECCION' => 'required',
        'TELEFONO' => 'required',
        'FECHA_DE_NACIMIENTO' => 'required',
        'EMAIL' => 'required',
       ]);
       $cliente = new cliente;
       $cliente ->NOMBRE    = $request-> NOMBRE;
       $cliente ->APELLIDOS = $request-> APELLIDOS;
       $cliente ->CEDULA    = $request-> CEDULA;
       $cliente ->DIRECCION = $request-> DIRECCION;
       $cliente ->TELEFONO  = $request-> TELEFONO;
       $cliente ->FECHA_DE_NACIMIENTO = $request-> FECHA_DE_NACIMIENTO;
       $cliente ->EMAIL    = $request-> EMAIL;
       $cliente->save();
       return redirect()->route('List.cliente');
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
