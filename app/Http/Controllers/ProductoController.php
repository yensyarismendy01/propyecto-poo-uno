<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\producto;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function InicioProducto(Request $request)
    {
    
       $producto = producto::all();
       return view('producto.inicio') -> with('producto',$producto);
    }

    public function CrearProducto(Request $request)
    {
       $producto = producto::all();
       return view('producto.crear')->with('producto',$producto);
    }

    public function GuardarProducto(Request $request)
    {
       $this->validate($request,[
        'NOMBRE' => 'required',
        'TIPO' => 'required',
        'ESTADO' => 'required',
        'PRECIO' => 'required',
       ]);
       $producto = new producto;
       $producto ->NOMBRE  = $request-> NOMBRE;
       $producto ->TIPO    = $request-> TIPO;
       $producto ->ESTADO  = $request-> ESTADO;
       $producto ->PRECIO  = $request-> PRECIO;
      
       $producto->save();
       return redirect()->route('List.producto');
    }

    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
