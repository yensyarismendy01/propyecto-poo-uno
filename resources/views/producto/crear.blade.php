@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"> CREAR PRODUCTOS</div>
                    <div class="col text-rig">
                        <a href="{{route ('List.producto')}}" class="btn btn-sm btn-primary">CANCELAR</a>
                            <div class="card-body">
                                <form role="form" method="post" action="{{route('guardar.producto') }}" >
                                  {{csrf_field()}}
                                  {{method_field ('post')}}

                                    <div class="col-lg-4">
                                        <div class="row">
                                         <label class="from-control-label" form="NOMBRE">NOMBRE DEL PRODUCTO</label>
                                         <input type="text" class="from-control" name="NOMBRE">
                                        </div></div>

                                        <div class="col-lg-4">
                                        <div class="row">
                                            <label class="from-control-label" form="TIPO">TIPO</label>
                                            <input type="text" class="from-control" name="TIPO">
                                        </div></div>

                                        <div class="col-lg-4">
                                        <div class="row">
                                            <label class="from-control-label" form="ESTADO">ESTADO</label>
                                            <input type="text" class="from-control" name="ESTADO">
                                        </div></div>

                                        <div class="col-lg-4">
                                       <div class="row">
                                            <label class="from-control-label" form="PRECIO" >PRECIO</label>
                                            <input type="text" class="from-control" name="PRECIO">
                                        </div></div>

                                  <button type= "submit" class="btn btn-sm btn-primary">GUARDAR</button>
                               
                                </form>
                        </div>      
                    </div>
         </div> 
     </div>
    </div>
</div>
@endsection