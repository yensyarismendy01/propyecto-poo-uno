@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"> MODULO DE CLIENTES </div>
                    <div class="col text-rig">
                        <a href="{{route('crear.cliente')}}" class="btn btn-sm btn-primary">CREAR CLIENTE</a>
                    </div>
                
                <div class="card-body">
                <table class="table">
                    <thead>
                    <tr>
                    <th scope="col">id</th>
                    <th scope="col">NOMBRE</th>
                    <th scope="col">APELLIDOS</th>
                    <th scope="col">CEDULA</th>
                    <th scope="col">DIRECCION</th>
                    <th scope="col">TELEFONO</th>
                    <th scope="col">FECHA_DE_NACIMIENTO</th>
                    <th scope="col">EMAIL</th>
                    </tr>
                    </thead>
  <tbody>
    @foreach  ($cliente as $item)
    <tr>
      <th scope="row">{{$item->id}}</th>
      <td>{{$item->NOMBRE}}</td>
      <td>{{$item->APELLIDOS}}</td>
      <td>{{$item->CEDULA}}</td>
      <td>{{$item->DIRECCION}}</td>
      <td>{{$item->TELEFONO}}</td>
      <td>{{$item->FECHA_DE_NACIMIENTO}}</td>
      <td>{{$item->EMAIL}}</td>
    
    </tr>
@endforeach
  </tbody>
</table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
