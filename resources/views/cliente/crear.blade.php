@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header"> CREAR CLIENTES</div>
                    <div class="col text-rig">
                        <a href="{{route ('List.cliente')}}" class="btn btn-sm btn-primary">CANCELAR</a>
                            <div class="card-body">
                                <form role="form" method="post" action="{{route('guardar.cliente') }}" >
                                  {{csrf_field()}}
                                  {{method_field ('post')}}

                                    <div class="col-lg-4">
                                        <div class="row">
                                         <label class="from-control-label" form="NOMBRE">NOMBRE DEL CLIENTE</label>
                                         <input type="text" class="from-control" name="NOMBRE">
                                        </div></div>
                                        <div class="col-lg-4">
                                        <div class="row">
                                            <label class="from-control-label" form="APELLIDOS">APELLIDOS DEL CLIENTE</label>
                                            <input type="text" class="from-control" name="APELLIDOS">
                                        </div></div>
                                        <div class="col-lg-4">
                                        <div class="row">
                                            <label class="from-control-label" form="CEDULA" > CEDULA DE CLIENTE</label>
                                            <input type="text" class="from-control" name="CEDULA">
                                        </div></div>
                                        <div class="col-lg-4">
                                       <div class="row">
                                            <label class="from-control-label" form="DIRECCION" > DIRECCION DEL CLIENTE</label>
                                            <input type="text" class="from-control" name="DIRECCION">
                                        </div></div>
                                        <div class="col-lg-4">
                                        <div class="row">
                                            <label class="from-control-label" form="TELEFONO" > TELEFONO DEL CLIENTE</label>
                                            <input type="text" class="from-control" name="TELEFONO">
                                        </div></div>
                                        <div class="col-lg-4">
                                        <div class="row">
                                            <label class="from-control-label" form="FECHA_DE_NACIMIENTO" > FECHA DE NACIMIENTO DEL CLIENTE  </label>
                                            <input type="date" class="from-control" name="FECHA_DE_NACIMIENTO">
                                        </div></div>
                                        <div class="col-lg-4">
                                        <div class="row">
                                            <label class="from-control-label" form="EMAIL" > EMAIL DEL CLIENTE  </label>
                                            <input type="text" class="from-control" name="EMAIL">
                                        </div>
                                        </div>
                                
                                  <button type= "submit" class="btn btn-sm btn-primary">GUARDAR</button>
                                </form>
                        </div>      
                    </div>
         </div> 
     </div>
    </div>
</div>
@endsection