@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('Escoja la funcionalidad que necesite!') }}

                    <div class="col text-rig">
                        <a href="{{route('List.cliente')}}" class="btn btn-sm btn-primary">LISTAR CLIENTES</a>
                        <a href="{{route('crear.cliente')}}" class="btn btn-sm btn-primary">CREAR CLIENTES</a>
                        <a href="{{route('List.producto')}}" class="btn btn-sm btn-primary">LISTAR PRODUCTOS</a>
                        <a href="{{route('crear.producto')}}" class="btn btn-sm btn-primary">CREAR PRODUCTOS</a>
                           </div>
  
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
